const limitFunctionFile = require('../limitFunctionCallCount');


const printIRan = () => {
    console.log("I RAN");
}

const printIRanToo = () => {
    console.log("I RAN TOO");
}

const testingVariableIRan = limitFunctionFile.limitFunctionCallCount(printIRan, 3);
const testingVariableIRanToo = limitFunctionFile.limitFunctionCallCount(printIRanToo, 2);

testingVariableIRan();//called  3 times remaining 2
testingVariableIRanToo();//called  2 times remaining 1
testingVariableIRan();//called  3 times remaining 1
testingVariableIRanToo();//called  2 times remaining 0
testingVariableIRan();//called  3 times remaining 0
testingVariableIRanToo();//called  2 times remaining null no output
testingVariableIRan();//called  3 times remaining null no output
testingVariableIRanToo();//called  2 times remaining null no output
testingVariableIRan();//called  3 times remaining null no output