const counterFile = require('../counterFactory')

const testerVariableOne = counterFile.counterFactory()

//default value 10
console.log(testerVariableOne.increment())//O/p 11
console.log(testerVariableOne.increment())//O/p 12
console.log(testerVariableOne.increment())//O/p 13
console.log(testerVariableOne.decrement())//O/p 12
console.log(testerVariableOne.decrement())//O/p 11
console.log(testerVariableOne.increment())//O/p 12
